﻿using FizzBuzzService.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzApp.Tests.DayOfWeekRuleTestCase
{
    [TestFixture]
    public class DayOfWeekRuleTest
    {
        private Mock<IDayCheck> mockObjectDayCheck;

        [SetUp]
        public void SetUp()
        {
            mockObjectDayCheck = new Mock<IDayCheck>();
        }

       [TestCase(DayOfWeek.Wednesday,true)]
       [TestCase(DayOfWeek.Tuesday, false)]

        public void IfDayMatches_Return_True_If_Input_Day_Is_Wednesday_Else_False(DayOfWeek dayOfWeek, bool isMatchfound)
        {
            mockObjectDayCheck.Setup(x => x.IfDayMatches(dayOfWeek)).Returns(isMatchfound);

            var actualResult = mockObjectDayCheck.Object.IfDayMatches(dayOfWeek);

            Assert.AreEqual(isMatchfound, actualResult);

        }

        [TearDown]
        public void TearDown()
        {
            mockObjectDayCheck = null;
        }

    }
}
