﻿using FizzBuzzApp.Models;
using FizzBuzzService.Interfaces;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FizzBuzzApp.Controllers
{
    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzService fizzBuzzService;

        public FizzBuzzController(IFizzBuzzService fizzBuzzService) 
        {
            this.fizzBuzzService = fizzBuzzService;
        }

        public ActionResult Index()
        {
            return View();
        }
       
        public ActionResult ResultList(FizzBuzzModel fizzBuzzModel, int? page)
        {
            if(ModelState.IsValid)
            {
                int pageNumber = page != null ? Convert.ToInt32(page) : 1;
                int pageSize = 10;
                var fizzbuzzList = this.fizzBuzzService.GetFizzBuzzList(fizzBuzzModel.Number);
                if (fizzbuzzList!= null)
                {
                    fizzBuzzModel.resultList = fizzbuzzList.ToPagedList(pageNumber, pageSize);
                }
            }
            return View("Index",fizzBuzzModel);
        }

    }
}