﻿using FizzBuzzService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FizzBuzzService.FizzBuzzServices
{
    public class FizzBuzzService:IFizzBuzzService
    {
        private readonly IEnumerable<IDivisiblityCheck> divisiblityCheck;

        public FizzBuzzService(IEnumerable<IDivisiblityCheck> divisiblityCheck)
        {
            this.divisiblityCheck = divisiblityCheck;
        }

        public List<string> GetFizzBuzzList(int inputNumber)
        {
            List<string> resultList = new List<string>();

            for (int iterator = 1; iterator <= inputNumber; iterator++)
            {
                var isDivisible = divisiblityCheck.Where(m => m.IsDivisible(iterator)).ToList();

                if (isDivisible.Any())
                {
                    resultList.Add(string.Join(" ", isDivisible.Select(m => m.GetMessage())));
                }
                else
                {
                    resultList.Add(iterator.ToString());
                }
            }

            return resultList;
        }
    }
}
