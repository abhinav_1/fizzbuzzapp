﻿using FizzBuzzService.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzzService.DivisiblityRule
{
    public class DivisiblityByThree:IDivisiblityCheck
    {
        private readonly IDayCheck dayOfWeekCheck;

        public DivisiblityByThree(IDayCheck dayCheck)
        {
            this.dayOfWeekCheck = dayCheck; // Do we need to keep same both
        }

        public bool IsDivisible(int number)
        {
            return number % 3 == 0;
        }

        public string GetMessage()
        {
            return dayOfWeekCheck.IfDayMatches(DateTime.Now.DayOfWeek) ? "Wizz" : "Fizz";
        }
    }
}
