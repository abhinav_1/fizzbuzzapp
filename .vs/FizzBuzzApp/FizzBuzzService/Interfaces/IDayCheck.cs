﻿namespace FizzBuzzService.Interfaces
{
    using System;
    public interface IDayCheck
    {
        bool IfDayMatches(DayOfWeek dayOfWeek);
    }
}
