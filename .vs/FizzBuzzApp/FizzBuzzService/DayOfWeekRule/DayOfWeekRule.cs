﻿using FizzBuzzService.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzzService.DayOfWeekRule
{
    public class DayOfWeekRule:IDayCheck
    {
        public bool IfDayMatches(DayOfWeek dayOfWeek)
        {
            return DayOfWeek.Wednesday == dayOfWeek ? true : false;
        }
    }
}
