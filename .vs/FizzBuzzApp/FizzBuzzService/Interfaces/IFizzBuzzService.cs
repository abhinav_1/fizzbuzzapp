﻿using System.Collections.Generic;

namespace FizzBuzzService.Interfaces
{
    public interface IFizzBuzzService
    {
        List<string> GetFizzBuzzList(int number);
    }
}
