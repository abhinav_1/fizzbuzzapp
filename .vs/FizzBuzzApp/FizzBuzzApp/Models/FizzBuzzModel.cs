﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FizzBuzzApp.Models
{
    public class FizzBuzzModel
    {
        [Required]
        [Range(1, 1000,ErrorMessage ="Please enter a number between 0 and 1000")]   
        [RegularExpression("^[0-9]*$",ErrorMessage ="Number must be numeric")]
        public int Number { get; set; }
        public IPagedList<string> resultList { get; set;}
    }
}