﻿namespace FizzBuzzService.Interfaces
{
    public interface IDivisiblityCheck
    {
        bool IsDivisible(int number);
        string GetMessage();
    }
}
