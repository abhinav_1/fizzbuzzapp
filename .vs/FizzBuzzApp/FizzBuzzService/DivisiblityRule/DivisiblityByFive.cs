﻿using FizzBuzzService.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzzService.DivisiblityRule
{
    public class DivisiblityByFive: IDivisiblityCheck
    {
        private readonly IDayCheck dayOfWeekCheck;

        public DivisiblityByFive(IDayCheck dayCheck)
        {
            this.dayOfWeekCheck = dayCheck; // Do we need to keep same both
        }

        public bool IsDivisible(int number)
        {
            return number % 5 == 0;
        }

        public string GetMessage()
        {
            return dayOfWeekCheck.IfDayMatches(DateTime.Now.DayOfWeek) ? "Wuzz" : "Buzz";
        }
    }
}
